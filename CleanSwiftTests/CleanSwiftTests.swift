//
//  CleanSwiftTests.swift
//  CleanSwiftTests
//
//  Created by Daniel Sevald on 11/11/2019.
//  Copyright © 2019 Daniel Sevald. All rights reserved.
//

import XCTest
@testable import CleanSwift

class CleanSwiftTests: XCTestCase {

    override func setUp() {
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }

    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }

    func testExample() {
        // This is an example of a functional test case.
        // Use XCTAssert and related functions to verify your tests produce the correct results.
        var hello: String?
        var buleano: Bool?
        
        hello = "Hola"
        XCTAssertEqual(hello, "Hola")
        
        buleano = false
        XCTAssertTrue(!buleano!)
    }

    func testPerformanceExample() {
        // This is an example of a performance test case.
        self.measure {
            // Put the code you want to measure the time of here.
        }
    }

}
