//
//  UserViewModel.swift
//  CleanSwift
//
//  Created by Daniel Sevald on 12/11/2019.
//  Copyright © 2019 Daniel Sevald. All rights reserved.
//

import Foundation

struct UserViewModel {
    let name: String
    let email: String
}
