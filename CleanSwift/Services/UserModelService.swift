//
//  UserModelService.swift
//  CleanSwift
//
//  Created by Daniel Sevald on 15/11/2019.
//  Copyright © 2019 Daniel Sevald. All rights reserved.
//

import Foundation

protocol UserModelServiceProtocol {
    func getUsers(completion: @escaping ([UserModel]) -> Void)
}

class UserModelService: UserModelServiceProtocol {
    var userService: UserService?
    var completion: (([UserModel]) -> Void)?
    
    func getUsers(completion: @escaping ([UserModel]) -> Void) {
        userService = UserService(delegate: self)
        self.completion = completion
        userService?.getUsers(completion: self.completion!)
    }
}

extension UserModelService: UserServiceDelegate {
    func getUserDidFinish(usersModel: [UserModel]) {
        completion?(usersModel)
    }
}
