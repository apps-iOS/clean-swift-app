//
//  UserService.swift
//  CleanSwift
//
//  Created by Daniel Sevald on 12/11/2019.
//  Copyright © 2019 Daniel Sevald. All rights reserved.
//

import Foundation

protocol UserServiceProtocol {
    func getUsers(completion: @escaping ([UserModel]) -> Void)
}

protocol UserServiceDelegate: class {
    func getUserDidFinish(usersModel: [UserModel])
}

class UserService: UserServiceProtocol {
    weak var delegate: UserServiceDelegate?
    let apiUrl = "https://jsonplaceholder.typicode.com/users"
    
    init(delegate: UserServiceDelegate) {
        self.delegate = delegate
    }
    
    func getUsers(completion: @escaping ([UserModel]) -> Void) {
        guard let url = URL(string: self.apiUrl) else { return }
        let session = URLSession.shared
        session.dataTask(with: url) { (data, response, error) in
            if let data = data {
                do {
                    let decoder = try JSONDecoder().decode([UserModel].self, from: data)
                    DispatchQueue.main.async {
                        self.delegate?.getUserDidFinish(usersModel: decoder)
                        //completion(decoder)
                    }
                } catch {
                    print(error)
                }
            }
        }.resume()
    }
}
