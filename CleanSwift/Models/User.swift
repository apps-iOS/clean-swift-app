//
//  User.swift
//  CleanSwift
//
//  Created by Daniel Sevald on 12/11/2019.
//  Copyright © 2019 Daniel Sevald. All rights reserved.
//

import Foundation

struct UserModel: Codable {
    let name: String
    let email: String
}

extension UserModel: Equatable {
    static func == (lhs: UserModel, rhs: UserModel) -> Bool {
        return lhs.email == rhs.email && lhs.name == rhs.name
    }
}
